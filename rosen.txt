// TMW2 scripts.
// Author:
//    Saulc
// Description:
//    Candor Armor&Weapon shop keeper.
// Variables:
//    CandorQuest_Rosen (nyi)
//      Suggestion: Deliver a letter to Zegas, giving player background about
//      Candor Island and Saxso. Requires level 5. Reward: 150 GP.
//      Could have an additional step related to Bifs. Even a daily quest asking
//      for (day % 8) ore, with suitable prices.

005-4,29,36,0	script	Rosen	NPC_GUARD1,{
    .@q = getq(ShipQuests_ChefGado);
    .@p = getq(CandorQuest_Rosen);

    function explain_ironingot {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Did you see Jhedia the blacksmith in Tulimshar? She might know how you could get this."),
            l("Nevertheless, you probably need some base materials from Bifs. Who knows what it will drop if you are lucky?");

        return;
    function explain_ironingot {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Did you see Jhedia the blacksmith in Tulimshar? She might know how you could get this."),
            l("Nevertheless, you probably need some base materials from Bifs. Who knows what it will drop if you are lucky?");

        return;
    }

    speech S_LAST_NEXT,
        l("Welcome to Tolchi and Rosen Shop."),
        l("What would you like today?");

    do
    {
        select
            menuaction(l("Trade")),
            l("How can I get iron ingot?"),
            l("Can you give me a task?"),
            menuaction(l("Quit"));

        switch (@menu)
        {
            case 1:
                closedialog;
                shop "Shop#Candor";
                close;
            case 2:
                explain_ironingot;
                break;
            case 2:
                explain_ironingot;
                break;
            case 4:
                closedialog;
                goodbye;
                close;
        }
    } while (1);

OnTimer1000:
    domovestep;

OnInit:
    initpath "move", 28, 36,
             "dir", DOWN, 0,
             "wait", 31, 0,
             "move", 31, 36,
             "dir", DOWN, 0,
             "wait", 31, 0,
             "move", 25, 35,
             "dir", UP, 0,
             "wait", 2, 0,    
             "move", 29, 36,
             "dir", DOWN, 0,
             "wait", 31, 0;       
    initialmove;
    initnpctimer;
    .distance = 5;
}             

    .@q = getq(ShipQuests_ChefGado);
    .@p = getq(CandorQuest_Rosen);

    // Player should have the rusty knife already
    if (!.@q) goto L_Gloves;

    // Piou quest
    if (.@q == 1 && .@p == 0) goto L_PiouLegs;
    if (.@p == 1) goto L_Continue;
    if (.@p == 2) goto L_Complete;

    // An error happened!
    close;

L_Gloves:
    mesn;
    mesq l("I can't give you a task, you look too weak, I never saw someone mine without gloves. Go talk to Chef Gado in ship, he should have another pair of gloves.");
    close;

L_Mine:
    mesn;
    mesq l("In forge we need ressources, Best way to obtain them it's to mine Bif's, Would you like to mine some for me?");
    mes "";
    menu
        l("Yes."), L_Start,
        l("Where can i found Bif ???"), L_Ask,
        l("No."), -;
    close;


L_Ask:
    mes "";
    mesn;
    mesq l("I have a spare pair of gloves laying somewhere, you can have those if you finish the task.");
    mes "";
    menu
        l("Sounds good."), L_Start,
        l("No thanks."), -;
    close;

L_Start:
    mes "";
    mesn;
    mesq l("Great, I need 11 @@. Remember a good food makes a good crew.",getitemlink("PiouLegs"));
    setq ShipQuests_ChefGado, 1;
    mes "";
    menu
        l("I'll get to work."), -;
    close;


L_Continue:
    setq ShipQuests_ChefGado, 1;
    mesn;
    mesq l("Do you have the @@/11 @@ I requested? Sailors are getting hungry because you!",countitem("PiouLegs"),getitemlink("PiouLegs"));
    mes "";
    menu
        rif(countitem("PiouLegs") >= 11, l("Yes, take them.")), L_Reward,
        l("Not yet."), -;
    close;


L_Reward:
    inventoryplace CreasedGloves, 1;
    delitem PiouLegs, 11;
    getitem CreasedGloves, 1;
    setq ShipQuests_ChefGado,2;
    Zeny = Zeny + 100;
    getexp 25, 5;
    mes "";
    mesn;
    mesq l("Thanks. Take this spare pair of gloves and some change.");
    next;
    mesq l("I'm sure the crew will like to be spared from having to eat @@ again!", getitemlink(Piberries));
    close;
    end;

L_Complete:
    .@r = rand(3);
    if (.@r == 0) npctalk3 l("What are you doing in my kitchen?! Get out, it's not a place for kids!");
    if (.@r == 1) npctalk3 l("Where is the salt?! This is sugar! Proper sailors need salt, not sugar!");
    if (.@r == 2) npctalk3 l("Are you going to stand here all day long? Go wash the dishes or go away.");
    closedialog;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 4;
    end;
}
